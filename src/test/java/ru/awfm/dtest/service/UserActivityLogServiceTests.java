package ru.awfm.dtest.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static ru.awfm.dtest.enums.StepEnum.YEAR;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserActivityLogServiceTests {

	@Autowired
	UserActivityLogServiceImpl activityLogService;

	@Test
	public void getActivitiesByYear() throws SQLException {
		Calendar fromDate = Calendar.getInstance();
		fromDate.set(2017, Calendar.FEBRUARY, 23, 0,0,0);
		Calendar toDate = Calendar.getInstance();
		toDate.set(2018, Calendar.MARCH, 23,0,0,0);
		List<Long> users = new ArrayList<>();
		users.add(1L);
		users.add(2L);
		activityLogService.getActivities(users, fromDate.getTime(), toDate.getTime(), YEAR);
	}

	@Test
	public void getPeriodsForYear() {
		Calendar fromDate = Calendar.getInstance();
		fromDate.set(2017, Calendar.FEBRUARY, 23, 0,0,0);
		Calendar toDate = Calendar.getInstance();
		toDate.set(2018, Calendar.MARCH, 23,0,0,0);

		List<Date> dates = activityLogService.getPeriods(fromDate.getTime(), toDate.getTime(), Calendar.DAY_OF_YEAR, Calendar.YEAR);
		assertEquals(2, dates.size());
	}


	@Test
	public void getPeriodsForMonth() {
		Calendar fromDate = Calendar.getInstance();
		fromDate.set(2017, Calendar.FEBRUARY, 23, 0,0,0);
		Calendar toDate = Calendar.getInstance();
		toDate.set(2018, Calendar.MARCH, 23,0,0,0);
		String step = "MONTH";

		List<Date> dates = activityLogService.getPeriods(fromDate.getTime(), toDate.getTime(), Calendar.DAY_OF_MONTH, Calendar.MONTH);
		assertEquals(14, dates.size());
	}

	@Test
	public void getPeriodsForWeek() {
		Calendar fromDate = Calendar.getInstance();
		fromDate.set(2017, Calendar.FEBRUARY, 23, 0,0,0);
		Calendar toDate = Calendar.getInstance();
		toDate.set(2017, Calendar.MARCH, 23,0,0,0);
		String step = "WEEK";

		List<Date> dates = activityLogService.getPeriods(fromDate.getTime(), toDate.getTime(),  Calendar.DAY_OF_WEEK, Calendar.WEEK_OF_YEAR);
		assertEquals(5, dates.size());
	}

	@Test
	public void getPeriodsForDay() {
		Calendar fromDate = Calendar.getInstance();
		fromDate.set(2017, Calendar.FEBRUARY, 23, 0,0,0);
		Calendar toDate = Calendar.getInstance();
		toDate.set(2017, Calendar.MARCH, 1,0,0,0);
		String step = "DAY";

		List<Date> dates = activityLogService.getPeriods(fromDate.getTime(), toDate.getTime(), -1, Calendar.DAY_OF_YEAR);
		assertEquals(7, dates.size());
	}

	@Test
	public void getPeriodsForHour() {
		Calendar fromDate = Calendar.getInstance();
		fromDate.set(2017, Calendar.FEBRUARY, 28, 0,0,0);
		Calendar toDate = Calendar.getInstance();
		toDate.set(2017, Calendar.MARCH, 1,23,0,0);

		List<Date> dates = activityLogService.getPeriods(fromDate.getTime(), toDate.getTime(), -1, Calendar.HOUR_OF_DAY);
		assertEquals(48, dates.size());
	}
}
