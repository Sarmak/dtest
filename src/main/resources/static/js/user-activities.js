/**
 * Created by Кирилл on 24.12.2017.
 */
$(document).ready(function () {
    getAllUsers();

    $("#search").click(function () {
        search();
    });

});

function getAllUsers() {
    $.ajax({
        url: "/api/users",
        type: "GET",
        success: function (data) {
            $.each(data, function(key, value) {
                $('#users').append($("<option></option>")
                    .attr("value", value.id)
                    .text(value.username));
            });
            console.log("getALlUsers success: ", data);
        },
        error: function (error) {
            console.log("getAllUsers error : ", error);
            alert("We've got some error: " + error.responseText);
        }
    });
}

function search() {
    var table = $("#user-activity");
    table.empty();
    var users = $('#users option:selected').map(function() { return $(this).val(); }).get();
    if (users.length !== 0) {
        var search = {
            fromDate: $("#fromDate").val(),
            toDate: $("#toDate").val(),
            users: users,
            step: $('#steps option:selected').val()
        };

        $.ajax({
            url: "/api/users/activities",
            type: "POST",
            data: JSON.stringify(search),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                fillTable(data);
            },
            error: function (error) {
                alert(error.status);
            }
        });
    } else {
        alert("Can you select users?");
    }
}

function fillTable(data) {
    var table = $("#user-activity");
    var tableRows = '';
    tableRows += '<tr><th>Период</th>';

    var user = {};
    var users = $('#users option').map(function() {
        return user[this.value] = this.innerHTML;
    }).get();

    $.each(data.userActivities, function(key, value) {
        tableRows += '<th>' + users[key-1] + '</th>';
    });
    tableRows += '</tr>';

    for (var i = 0; i < data.dates.length; i++) {
        tableRows += '<tr>' +
            '<td>' + data.dates[i] + '</td>';
        $.each(data.userActivities, function(key, value) {
            tableRows += '<td>' + value[i] + '</td>';
        });
        tableRows += '</tr>';
    }
    table.append(tableRows);
}