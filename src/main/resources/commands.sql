--�������� ���������
CREATE SCHEMA IF NOT EXISTS dtest;

CREATE TABLE dtest.T_USER
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_name VARCHAR(255) NOT NULL
);
CREATE UNIQUE INDEX T_USER_user_name_uindex ON dtest.T_USER (user_name);

CREATE TABLE dtest.t_user_activity_log
(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    activity_date DATETIME NOT NULL COMMENT '���� � ����� ����������',
    activity_count INT NOT NULL COMMENT '���������� �������� ������������ � �������� ������� �������',
    CONSTRAINT t_user_activity_log_user_fk FOREIGN KEY (user_id) REFERENCES t_user (id)
);

--���������� ���������
insert into t_user (user_name) values ('Ivanov');
insert into t_user (user_name) values ('Petrov');
insert into t_user (user_name) values ('Sidorov');

insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-10-23 10:00:00', 1);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-10-23 10:15:00', 1);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-10-23 11:00:00', 2);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-10-23 12:00:00', 3);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-10-24 10:00:00', 4);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-10-25 10:00:00', 3);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-10-26 13:00:00', 7);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-11-26 12:00:00', 5);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-12-26 13:00:00', 3);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2017-09-26 14:00:00', 2);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2018-09-26 14:00:00', 2);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (1, '2019-09-26 12:00:00', 12);
insert into t_user_activity_log (user_id, activity_date, activity_count) values (2, '2017-10-23 14:00:00', 5);


