package ru.awfm.dtest.config;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Кирилл on 23.12.2017.
 */

@Configuration
public class DatabaseConfig {

    @Value("${mysql.db.url}")
    private String url;
    @Value("${mysql.db.username}")
    private String username;
    @Value("${mysql.db.password}")
    private String password;

    @Bean
    public MysqlDataSource getMySQLDataSource() {
        MysqlDataSource mysqlDS = new MysqlDataSource();
        mysqlDS.setURL(url);
        mysqlDS.setUser(username);
        mysqlDS.setPassword(password);
        return mysqlDS;
    }
}
