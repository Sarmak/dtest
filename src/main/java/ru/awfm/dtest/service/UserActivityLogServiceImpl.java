package ru.awfm.dtest.service;

import org.springframework.stereotype.Service;
import ru.awfm.dtest.dto.UserActivityDTO;
import ru.awfm.dtest.enums.StepEnum;
import ru.awfm.dtest.repository.UserActivityLogRepository;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Кирилл on 23.12.2017.
 */

@Service
public class UserActivityLogServiceImpl implements UserActivityLogService {

    private final UserActivityLogRepository activityLogRepository;

    public UserActivityLogServiceImpl(UserActivityLogRepository activityLogRepository) {
        this.activityLogRepository = activityLogRepository;
    }

    @Override
    public UserActivityDTO getActivities(List<Long> users, Date fromDate, Date toDate, StepEnum step) throws SQLException {
        List<Date> dates;
        Map<Long, List<Long>> userActivities;
        switch (step) {
            case HOUR: {
                Calendar toDateCnd = Calendar.getInstance();
                toDateCnd.setTime(toDate);
                //set to the end of toDate.
                toDateCnd.set(Calendar.HOUR_OF_DAY, 23);
                dates = getPeriods(fromDate, toDateCnd.getTime(), -1, Calendar.HOUR_OF_DAY);
                userActivities = activityLogRepository.getActivitiesByHour(users, fromDate, toDate, dates);
                break;
            }
            case DAY: {
                dates = getPeriods(fromDate, toDate, -1, Calendar.DAY_OF_YEAR);
                userActivities = activityLogRepository.getActivitiesByDay(users, fromDate, toDate, dates);
                break;
            }
            case WEEK: {
                dates = getPeriods(fromDate, toDate, Calendar.DAY_OF_WEEK, Calendar.WEEK_OF_YEAR);
                userActivities = activityLogRepository.getActivitiesByWeek(users, fromDate, toDate, dates);
                break;
            }
            case MONTH: {
                dates = getPeriods(fromDate, toDate, Calendar.DAY_OF_MONTH, Calendar.MONTH);
                userActivities = activityLogRepository.getActivitiesByMonth(users, fromDate, toDate, dates);
                break;
            }
            case YEAR:
            default: {
                dates = getPeriods(fromDate, toDate, Calendar.DAY_OF_YEAR, Calendar.YEAR);
                userActivities = activityLogRepository.getActivitiesByYear(users, fromDate, toDate, dates);
                break;
            }
        }
        UserActivityDTO userActivityDTO = new UserActivityDTO();
        userActivityDTO.setUserActivities(userActivities);
        userActivityDTO.setDates(formatDates(dates, step));
        return userActivityDTO;
    }

    private List<String> formatDates(List<Date> dates, StepEnum step) {
        SimpleDateFormat sdf;
        List<String> formattedDates = new ArrayList<>();
        switch (step) {
            case HOUR:
                sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                break;
            case DAY:
                sdf = new SimpleDateFormat("dd.MM.yyyy");
                break;
            case WEEK:
                sdf = new SimpleDateFormat("dd.MM.yyyy");
                break;
            case MONTH:
                sdf = new SimpleDateFormat("MM.yyyy");
                break;
            case YEAR:
            default:
                sdf = new SimpleDateFormat("yyyy");
                break;
        }
        for (Date date : dates) {
            formattedDates.add(sdf.format(date));
        }
        return formattedDates;
    }

    List<Date> getPeriods(Date fromDate, Date toDate, int clearField, int addField) {
        Calendar currentDateCnd = Calendar.getInstance();
        currentDateCnd.setTime(fromDate);
        Calendar toDateCnd = Calendar.getInstance();
        toDateCnd.setTime(toDate);
        //no need to clear anything
        if (clearField != -1) {
            //all queries in mysql support SUNDAY in current modes. it'll fix russian locale.
            currentDateCnd.setFirstDayOfWeek(Calendar.SUNDAY);
            toDateCnd.setFirstDayOfWeek(Calendar.SUNDAY);
            currentDateCnd.set(clearField, 1);
            toDateCnd.set(clearField, 1);
        }

        List<Date> dates = new ArrayList<>();
        while (currentDateCnd.compareTo(toDateCnd) <= 0) {
            dates.add(currentDateCnd.getTime());
            currentDateCnd.add(addField, 1);
        }
        return dates;
    }
}
