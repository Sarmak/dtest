package ru.awfm.dtest.service;

import ru.awfm.dtest.dto.UserActivityDTO;
import ru.awfm.dtest.enums.StepEnum;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Кирилл on 23.12.2017.
 */
public interface UserActivityLogService {

    UserActivityDTO getActivities(List<Long> users, Date fromDate, Date toDate, StepEnum step) throws SQLException;

}
