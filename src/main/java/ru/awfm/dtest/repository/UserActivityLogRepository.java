package ru.awfm.dtest.repository;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Кирилл on 23.12.2017.
 */
public interface UserActivityLogRepository {

    Map<Long, List<Long>> getActivitiesByYear(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException;

    Map<Long, List<Long>> getActivitiesByMonth(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException;

    Map<Long, List<Long>> getActivitiesByWeek(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException;

    Map<Long, List<Long>> getActivitiesByDay(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException;

    Map<Long, List<Long>> getActivitiesByHour(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException;

}
