package ru.awfm.dtest.repository;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Кирилл on 23.12.2017.
 */
@Repository
public class UserActivityLogRepositoryImpl implements UserActivityLogRepository {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final MysqlDataSource dataSource;

    @Autowired
    public UserActivityLogRepositoryImpl(MysqlDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Map<Long, List<Long>> getActivitiesByYear(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException {
        Map<Long, List<Long>> userActivities = prepareUserActivitiesMap(users, dates);
        try (Connection connection = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder("SELECT\n" +
                    " year(ual.activity_date) year,\n" +
                    " u.id               userId,\n" +
                    " sum(activity_count)     activities\n" +
                    "FROM t_user_activity_log ual\n" +
                    " LEFT JOIN t_user u ON ual.user_id = u.id\n" +
                    "WHERE date(ual.activity_date) BETWEEN (?) AND (?)\n");
            query = appendUsers(query, users.size());
            query = appendActivityParameters(query, dates.size(), "year");
            query.append(" GROUP BY year(ual.activity_date), u.id\n" +
                        "ORDER BY year(ual.activity_date)");
            PreparedStatement statement = connection.prepareStatement(query.toString());
            statement.setDate(1, new java.sql.Date(fromDate.getTime()));
            statement.setDate(2, new java.sql.Date(toDate.getTime()));
            for(int i = 0; i < users.size(); i++) {
                statement.setLong(i+3, users.get(i));
            }
            for(int i = 0; i < dates.size(); i++) {
                statement.setString(i+3+users.size(), DATE_FORMAT.format(dates.get(i)));
            }
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                List<Long> activities = userActivities.get(rs.getLong("userId"));
                Calendar currentDate = Calendar.getInstance();
                currentDate.set(rs.getInt("year"), Calendar.JANUARY, 1, 0,0,0);
                currentDate.set(Calendar.MILLISECOND, 0);
                int index = dates.indexOf(currentDate.getTime());
                activities.set(index, rs.getLong("activities"));
            }
            statement.close();
        }
        return userActivities;
    }

    @Override
    public Map<Long, List<Long>> getActivitiesByMonth(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException {
        Map<Long, List<Long>> userActivities = prepareUserActivitiesMap(users, dates);
        try (Connection connection = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder("SELECT\n" +
                    "  month(ual.activity_date)    month,\n" +
                    "  year(ual.activity_date)       year,\n" +
                    "  u.id                    userId,\n" +
                    "  sum(activity_count)     activities\n" +
                    "FROM t_user_activity_log ual\n" +
                    " LEFT JOIN t_user u ON ual.user_id = u.id\n" +
                    "WHERE date(ual.activity_date) BETWEEN (?) AND (?)\n");
            query = appendUsers(query, users.size());
            query = appendActivityParameters(query, dates.size(), "month", "year");
            query.append(" GROUP BY year(ual.activity_date), month(ual.activity_date), u.id\n" +
                    "ORDER BY year(ual.activity_date), month(ual.activity_date)");
            PreparedStatement statement = connection.prepareStatement(query.toString());
            int paramIndex = 1;
            statement.setDate(paramIndex++, new java.sql.Date(fromDate.getTime()));
            statement.setDate(paramIndex++, new java.sql.Date(toDate.getTime()));
            for(Long userId : users) {
                statement.setLong(paramIndex++, userId);
            }
            for(Date date : dates) {
                statement.setString(paramIndex++, DATE_FORMAT.format(date));
                statement.setString(paramIndex++, DATE_FORMAT.format(date));
            }
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                List<Long> activities = userActivities.get(rs.getLong("userId"));
                Calendar currentDate = Calendar.getInstance();
                currentDate.set(rs.getInt("year"), rs.getInt("month") - 1, 1, 0,0,0);
                currentDate.set(Calendar.MILLISECOND, 0);
                int index = dates.indexOf(currentDate.getTime());
                activities.set(index, rs.getLong("activities"));
            }
            statement.close();
        }
        return userActivities;
    }

    @Override
    public Map<Long, List<Long>> getActivitiesByWeek(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException {
        Map<Long, List<Long>> userActivities = prepareUserActivitiesMap(users, dates);
        try (Connection connection = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder("SELECT\n" +
                    "  week(ual.activity_date)    week,\n" +
                    "  year(ual.activity_date)       year,\n" +
                    "  u.id                    userId,\n" +
                    "  sum(activity_count)     activities\n" +
                    "FROM t_user_activity_log ual\n" +
                    " LEFT JOIN t_user u ON ual.user_id = u.id\n" +
                    "WHERE date(ual.activity_date) BETWEEN (?) AND (?)\n");
            query = appendUsers(query, users.size());
            query = appendActivityParameters(query, dates.size(), "week", "year");
            query.append(" GROUP BY year(ual.activity_date), week(ual.activity_date), u.id\n" +
                    "ORDER BY year(ual.activity_date), week(ual.activity_date)");
            PreparedStatement statement = connection.prepareStatement(query.toString());
            int paramIndex = 1;
            statement.setDate(paramIndex++, new java.sql.Date(fromDate.getTime()));
            statement.setDate(paramIndex++, new java.sql.Date(toDate.getTime()));
            for(Long userId : users) {
                statement.setLong(paramIndex++, userId);
            }
            for(Date date : dates) {
                statement.setString(paramIndex++, DATE_FORMAT.format(date));
                statement.setString(paramIndex++, DATE_FORMAT.format(date));
            }
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                List<Long> activities = userActivities.get(rs.getLong("userId"));
                Calendar currentDate = Calendar.getInstance();
                currentDate.set(rs.getInt("year"), Calendar.JANUARY, 1, 0,0,0);
                currentDate.set(Calendar.MILLISECOND, 0);
                currentDate.set(Calendar.WEEK_OF_YEAR, rs.getInt("week"));
                currentDate.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                int index = dates.indexOf(currentDate.getTime());
                activities.set(index, rs.getLong("activities"));
            }
            statement.close();
        }
        return userActivities;
    }

    @Override
    public Map<Long, List<Long>> getActivitiesByDay(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException {
        Map<Long, List<Long>> userActivities = prepareUserActivitiesMap(users, dates);
        try (Connection connection = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder("SELECT\n" +
                    "  dayofyear(ual.activity_date)    day,\n" +
                    "  year(ual.activity_date)       year,\n" +
                    "  u.id                    userId,\n" +
                    "  sum(activity_count)     activities\n" +
                    "FROM t_user_activity_log ual\n" +
                    " LEFT JOIN t_user u ON ual.user_id = u.id\n" +
                    "WHERE date(ual.activity_date) BETWEEN (?) AND (?)\n");
            query = appendUsers(query, users.size());
            query = appendActivityParameters(query, dates.size(),"dayofyear", "year");
            query.append(" GROUP BY year(ual.activity_date), dayofyear(ual.activity_date), u.id\n" +
                    "ORDER BY year(ual.activity_date), dayofyear(ual.activity_date)");
            PreparedStatement statement = connection.prepareStatement(query.toString());
            int paramIndex = 1;
            statement.setDate(paramIndex++, new java.sql.Date(fromDate.getTime()));
            statement.setDate(paramIndex++, new java.sql.Date(toDate.getTime()));
            for(Long userId : users) {
                statement.setLong(paramIndex++, userId);
            }
            for(Date date : dates) {
                statement.setString(paramIndex++, DATE_FORMAT.format(date));
                statement.setString(paramIndex++, DATE_FORMAT.format(date));
            }
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                List<Long> activities = userActivities.get(rs.getLong("userId"));
                Calendar currentDate = Calendar.getInstance();
                currentDate.set(rs.getInt("year"), Calendar.JANUARY, 1, 0,0,0);
                currentDate.set(Calendar.MILLISECOND, 0);
                currentDate.set(Calendar.DAY_OF_YEAR, rs.getInt("day"));
                int index = dates.indexOf(currentDate.getTime());
                activities.set(index, rs.getLong("activities"));
            }
            statement.close();
        }
        return userActivities;
    }

    @Override
    public Map<Long, List<Long>> getActivitiesByHour(List<Long> users, Date fromDate, Date toDate, List<Date> dates) throws SQLException {
        Map<Long, List<Long>> userActivities = prepareUserActivitiesMap(users, dates);
        try (Connection connection = dataSource.getConnection()) {
            StringBuilder query = new StringBuilder("SELECT\n" +
                    "  hour(ual.activity_date) hour,\n" +
                    "  dayofyear(ual.activity_date)  day,\n" +
                    "  year(ual.activity_date)       year,\n" +
                    "  u.id                    userId,\n" +
                    "  sum(activity_count)     activities\n" +
                    "FROM t_user_activity_log ual\n" +
                    " LEFT JOIN t_user u ON ual.user_id = u.id\n" +
                    "WHERE date(ual.activity_date) BETWEEN (?) AND (?)\n");
            query = appendUsers(query, users.size());
            query = appendActivityParameters(query, dates.size(), "hour", "dayofyear", "year");
            query.append(" GROUP BY year(ual.activity_date), dayofyear(ual.activity_date), hour(ual.activity_date), u.id\n" +
                    "ORDER BY year(ual.activity_date), dayofyear(ual.activity_date), hour(ual.activity_date)");
            PreparedStatement statement = connection.prepareStatement(query.toString());
            int paramIndex = 1;
            statement.setDate(paramIndex++, new java.sql.Date(fromDate.getTime()));
            statement.setDate(paramIndex++, new java.sql.Date(toDate.getTime()));
            for(Long userId : users) {
                statement.setLong(paramIndex++, userId);
            }
            for(Date date : dates) {
                statement.setString(paramIndex++, DATE_TIME_FORMAT.format(date));
                statement.setString(paramIndex++, DATE_TIME_FORMAT.format(date));
                statement.setString(paramIndex++, DATE_TIME_FORMAT.format(date));
            }
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                List<Long> activities = userActivities.get(rs.getLong("userId"));
                Calendar currentDate = Calendar.getInstance();
                currentDate.set(rs.getInt("year"), Calendar.JANUARY, 1, rs.getInt("hour"),0,0);
                currentDate.set(Calendar.DAY_OF_YEAR, rs.getInt("day"));
                currentDate.set(Calendar.MILLISECOND, 0);
                int index = dates.indexOf(currentDate.getTime());
                activities.set(index, rs.getLong("activities"));
            }
            statement.close();
        }
        return userActivities;
    }

    private Map<Long, List<Long>> prepareUserActivitiesMap(List<Long> users, List<Date> dates) {
        Map<Long, List<Long>> userActivities = new HashMap<>(users.size());
        List<Long> defaultActivities = new ArrayList<>(dates.size());
        for (int i = 0; i < dates.size(); i++) {
            defaultActivities.add(0L);
        }
        for (Long userId : users) {
            userActivities.put(userId, new ArrayList<>(defaultActivities));
        }
        return userActivities;
    }

    private StringBuilder appendUsers(StringBuilder query, int usersSize) {
        if (usersSize > 0) {
            query.append("AND ual.user_id IN (");
            for (int i = 0; i < usersSize; i++) {
                query.append(" ?");
                if (i != usersSize - 1) query.append(",");
            }
            query.append(")");
        }
        return query;
    }

    private StringBuilder appendActivityParameters(StringBuilder query, int periodsSize, String... functions) {
        if (periodsSize > 0) {
            query.append(" AND (");
            for (int i = 0; i < periodsSize; i++) {
                query = appendActivityDateParameterForPeriod(query, functions);
                if (i != periodsSize - 1) query.append(" OR ");
            }
            query.append(")");
        }
        return query;
    }

    private StringBuilder appendActivityDateParameterForPeriod(StringBuilder query, String... functions) {
        for (String function : functions) {
            query = appendActivityDateParameter(query, function);
            query.append(" AND ");
        }
        query.setLength(query.length() - 5);
        return query;
    }

    private StringBuilder appendActivityDateParameter(StringBuilder query, String function){
        return query.append(function).append("(ual.activity_date) = ").append(function).append("(?)" );
    }
}
