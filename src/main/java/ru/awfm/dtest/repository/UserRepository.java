package ru.awfm.dtest.repository;

import ru.awfm.dtest.dto.UserDTO;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Кирилл on 23.12.2017.
 */
public interface UserRepository {

    List<UserDTO> findAll() throws SQLException;

}
