package ru.awfm.dtest.repository;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.awfm.dtest.dto.UserDTO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Кирилл on 23.12.2017.
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

    private final MysqlDataSource dataSource;

    @Autowired
    public UserRepositoryImpl(MysqlDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<UserDTO> findAll() throws SQLException {
        List<UserDTO> users = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet rs = statement.executeQuery("select * from t_user");
            while (rs.next()) {
                UserDTO user = new UserDTO(rs.getLong("id"), rs.getString("user_name"));
                users.add(user);
            }
        }
        return users;
    }
}
