package ru.awfm.dtest.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Кирилл on 24.12.2017.
 */
public class JsonDateDeserializer extends JsonDeserializer<Date> {

    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Date deserialize(JsonParser jsonparser,
                            DeserializationContext deserializationcontext) throws IOException, JsonProcessingException {

        String dateStr = jsonparser.getText();
        try {
            Date date = FORMAT.parse(dateStr);
            Calendar dateCnd = Calendar.getInstance();
            dateCnd.setTime(date);
            dateCnd.set(Calendar.HOUR, 0);
            dateCnd.set(Calendar.MINUTE, 0);
            dateCnd.set(Calendar.SECOND, 0);
            dateCnd.set(Calendar.MILLISECOND, 0);
            return dateCnd.getTime();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

    }


}
