package ru.awfm.dtest.dto;

import java.util.List;
import java.util.Map;

/**
 * Created by Кирилл on 24.12.2017.
 */
public class UserActivityDTO {

    private Map<Long, List<Long>> userActivities;
    private List<String> dates;

    public UserActivityDTO() {
    }

    public UserActivityDTO(Map<Long, List<Long>> userActivities, List<String> dates) {
        this.userActivities = userActivities;
        this.dates = dates;
    }

    public Map<Long, List<Long>> getUserActivities() {
        return userActivities;
    }

    public void setUserActivities(Map<Long, List<Long>> userActivities) {
        this.userActivities = userActivities;
    }

    public List<String> getDates() {
        return dates;
    }

    public void setDates(List<String> dates) {
        this.dates = dates;
    }
}
