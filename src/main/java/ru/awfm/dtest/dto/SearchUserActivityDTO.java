package ru.awfm.dtest.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import ru.awfm.dtest.enums.StepEnum;
import ru.awfm.dtest.util.JsonDateDeserializer;

import java.util.Date;
import java.util.List;

/**
 * Created by Кирилл on 24.12.2017.
 */
public class SearchUserActivityDTO {
    private List<Long> users;
    @JsonDeserialize(using = JsonDateDeserializer.class)
    private Date fromDate;
    @JsonDeserialize(using = JsonDateDeserializer.class)
    private Date toDate;
    private StepEnum step;

    public List<Long> getUsers() {
        return users;
    }

    public void setUsers(List<Long> users) {
        this.users = users;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public StepEnum getStep() {
        return step;
    }

    public void setStep(StepEnum step) {
        this.step = step;
    }
}
