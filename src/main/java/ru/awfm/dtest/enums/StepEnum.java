package ru.awfm.dtest.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Кирилл on 24.12.2017.
 */
public enum StepEnum {
    @JsonProperty("HOUR")
    HOUR,
    @JsonProperty("DAY")
    DAY,
    @JsonProperty("WEEK")
    WEEK,
    @JsonProperty("MONTH")
    MONTH,
    @JsonProperty("YEAR")
    YEAR
}
