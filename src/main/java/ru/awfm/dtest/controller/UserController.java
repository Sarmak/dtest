package ru.awfm.dtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.awfm.dtest.dto.SearchUserActivityDTO;
import ru.awfm.dtest.dto.UserActivityDTO;
import ru.awfm.dtest.dto.UserDTO;
import ru.awfm.dtest.repository.UserRepository;
import ru.awfm.dtest.service.UserActivityLogService;

import java.sql.SQLException;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

/**
 * Created by Кирилл on 23.12.2017.
 */

@RestController
@RequestMapping(value="/api/users")
public class UserController {

    private final UserRepository userRepository;
    private final UserActivityLogService activityLogService;

    @Autowired
    public UserController(UserRepository userRepository, UserActivityLogService activityLogService) {
        this.userRepository = userRepository;
        this.activityLogService = activityLogService;
    }

    @GetMapping
    public ResponseEntity<List<UserDTO>> findAll() throws SQLException {
        return ok(userRepository.findAll());
    }

    @PostMapping(value="/activities")
    public ResponseEntity<UserActivityDTO> getActivities(@RequestBody SearchUserActivityDTO parameters) throws SQLException {
        return ok(activityLogService.getActivities(parameters.getUsers(),
                parameters.getFromDate(),
                parameters.getToDate(),
                parameters.getStep()));
    }

}
