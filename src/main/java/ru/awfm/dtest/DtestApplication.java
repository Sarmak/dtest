package ru.awfm.dtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("ru.awfm.dtest")
public class DtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtestApplication.class, args);
	}
}
